document.addEventListener('DOMContentLoaded', function () {
  	app.init();
});

var app = {
  	
	coordenadas: null,
	wpid: null, 
	distanciaActual: 0,
	distanciaHoy: 0,
	precision: 80,
	fecha: new Date(),
    mes: "",
	hoy: "",
	inicio: true,
  	recorrido: document.getElementById('chronoDiv'),
  	play: document.getElementById('softkey-center'),
  	borrar: document.getElementById('softkey-left'),
  	borrarDia: document.getElementById('softkey-right'),
  	historial: document.getElementById('historial'),
  	principalDiv: document.getElementById('principalDiv'),
  	
	
	redondeo: function(numero, decimales) {
    	var flotante = parseFloat(numero);
    	var resultado = Math.round(flotante * Math.pow(10, decimales)) / Math.pow(10, decimales);
    	return resultado;
	},

	calcularDistancia: function(coordenadasOrigen, coordenadasDestino) {
	    var radianesLatInicio = app.gradosARadianes(coordenadasOrigen.latitude);
	    var radianesLongInicio = app.gradosARadianes(coordenadasOrigen.longitude);
	    var radianesLatDestino = app.gradosARadianes(coordenadasDestino.latitude);
	    var radianesLongDestino = app.gradosARadianes(coordenadasDestino.longitude);
	    var Radio = 6371; // radio de la Tierra en Km
	    var resultado = Math.acos(Math.sin(radianesLatInicio) * Math.sin(radianesLatDestino) +
	        Math.cos(radianesLatInicio) * Math.cos(radianesLatDestino) *
	        Math.cos(radianesLongInicio - radianesLongDestino)) * Radio;
	    return app.redondeo(resultado, 3);
	},

	gradosARadianes: function(grados) {
    	var radianes = (grados * Math.PI) / 180;
    	return radianes;
	},
  
  	init: function() {
    	document.addEventListener('keypress', (e) => {
				app.manejarTeclado(e);
			});


    	app.mes = (app.fecha.getMonth()+1); 
	    if(app.mes <10){
	        app.mes = 0+""+app.mes;
	    }
		app.hoy= app.fecha.getFullYear()+""+ app.mes +""+app.fecha.getDate();

    	if (localStorage.getItem("podo_"+app.hoy)) {
        	app.distanciaHoy = app.redondeo(localStorage.getItem("podo_"+app.hoy), 3);
    	} else {
        	app.distanciaHoy = 0;
        	localStorage.setItem("podo_"+app.hoy, app.distanciaHoy);
    	}    

    	app.mostrarHistorial();
  	},

  	mostrarHistorial: function() {    	
  		var historialTxt = ""

    	//mostrar localstorage menos hoy
        if (localStorage.length > 0) {
	        for (var i in localStorage) {
	            if (i.substring(0, 5) == "podo_" && i !== ('podo_'+app.hoy)) {
	            	//no mostrar hoy

	                var fecha = i.substr(5, i.length - 1);
	                var dist = localStorage.getItem(i);
	                historialTxt = fecha.substr(6, 2) + "/" + fecha.substr(4, 2) + "/" + fecha.substr(0, 4) + " &nbsp;" +dist + " km";
	            }
	        }
    	}
    	app.historial.innerHTML = historialTxt;
  	},

	ver1: function(posicion) {
		//console.log("ver1");

	    if (posicion.coords.accuracy <= app.precision &&
	        (posicion.coords.latitude != app.coordenadas.latitude || posicion.coords.longitude != app.coordenadas.longitude)) {

		//console.log("ver1 bueno");
	        var coordenadasNew = {
	            latitude: posicion.coords.latitude,
	            longitude: posicion.coords.longitude
	        }

	        var distanciaCalculada = parseFloat(app.calcularDistancia(app.coordenadas, coordenadasNew));
	        
	        //console.log(distanciaCalculada);

	        if (!isNaN(distanciaCalculada)) {
	            app.distanciaActual = app.redondeo(app.distanciaActual + distanciaCalculada, 3);
	            app.distanciaHoy = app.redondeo(app.distanciaHoy + distanciaCalculada, 3);
				app.recorrido.innerHTML = app.distanciaActual + " km ahora<br/>" +app.distanciaHoy + " km hoy";

				//guardar en localstorage app.distanciaHoy
				localStorage.setItem("podo_"+app.hoy, app.distanciaHoy);
        
	        }

	        app.coordenadas = {
	            latitude: posicion.coords.latitude,
	            longitude: posicion.coords.longitude
	        }	        
	    }
	},

	ver: function(posicion) {
		/*
		//console.log("ver");

  		app.recorrido.innerHTML = "Latitude: " + posicion.coords.latitude + "<br>Longitude: " + posicion.coords.longitude;
*/
	    app.coordenadas = {
	        latitude: posicion.coords.latitude,
	        longitude: posicion.coords.longitude
	    }

	    var geo_options = {
	        enableHighAccuracy: true,
	        maximumAge: 30000,
	        timeout: 27000
	    };


	    app.recorrido.innerHTML = app.distanciaActual + " km ahora<br/>" + app.distanciaHoy + " km hoy";
	    app.wpid = navigator.geolocation.watchPosition(app.ver1, null, geo_options);


		//guardar en localstorage app.distanciaHoy


	    //app.wpid = navigator.geolocation.watchPosition(app.ver1, null, null);
	},

	manejarTeclado: function(e) {
		if (e.key == "SoftLeft" && app.inicio) { 
			if (confirm("Borrar hoy?") == true) {
				//borrar localstorage solo de hoy
		        if (localStorage.length > 0) {
		            for (var i in localStorage) {
		                if (i === ('podo_'+app.hoy)) {
		                	//console.log("borrar hoy "+i);
		                    localStorage.removeItem(i);
		                }
		            }
		        }
		        app.mostrarHistorial();
			}
		}
		else if (e.key == "SoftRight" && app.inicio) { 
			// Clear 			
			if (confirm("Borrar todo?") == true) {
				//borrar localstorage
		        if (localStorage.length > 0) {
		            for (var i in localStorage) {
		                if (i.substring(0, 5) == "podo_") {
		                	//console.log("borrar "+ i);
		                    localStorage.removeItem(i);
		                }
		            }
		        }
		        app.mostrarHistorial();
			}
		}
		else if (e.key == "MicrophoneToggle"){ // || e.key == "Enter") {
		    if (app.inicio) {
		    	//console.log("iniciar tracker");
		        navigator.geolocation.getCurrentPosition(app.ver, null);
				
				/*
				var options = {
				  enableHighAccuracy: true,
				  timeout: 5000,
				  maximumAge: 0
				};
				navigator.geolocation.getCurrentPosition(app.ver, null, options);
				*/
		        
		        app.inicio = false;
		        app.play.innerHTML="Stop";

		        app.borrarDia.classList.add('grey');
		        app.borrar.classList.add('grey');
		        app.principalDiv.classList.add('active');
		    } else {
		    	//console.log("parar tracker");
		        app.inicio = true;
		        app.play.innerHTML="Start";

		        app.borrarDia.classList.remove('grey');
		        app.borrar.classList.remove('grey');
		        app.principalDiv.classList.remove('active');

		        app.distanciaActual = 0;

		        app.recorrido.innerHTML = app.distanciaHoy + " km hoy";

				//guardar en localstorage app.distanciaHoy
				//localStorage.setItem("podo_"+app.hoy, app.distanciaHoy);

		        if (app.wpid !== null && app.wpid !== undefined) {
		            //console.log("cerrar: " + app.wpid);
		            navigator.geolocation.clearWatch(app.wpid);
		        }
		    }
	    }
	}
};

document.addEventListener('DOMContentLoaded', function () {
  	app.init();
});

var app = {


  	valueDivs : ["workValueDiv", "minValueDiv", "maxValueDiv", "numberBeepDiv"],

  	workTimeElement: document.getElementById('workTime'),
  	workTime: 0,

  	setDiv: document.getElementById('setDiv'),
  	chronoDiv: document.getElementById('chronoDiv'),

  	workValue: document.getElementById('workValue'),
  	workValueInt: 60,

  	minValue: document.getElementById('minValue'),
  	minValueInt: 2,
  	
  	maxValue: document.getElementById('maxValue'),
  	maxValueInt: 4,

  	timeRest: document.getElementById('timeRest'),
  	timeRestInt: 0,

  	interval: null,
  	intervalButton: null,
  	timeBeep: null,
  	audio: new Audio(),
  	isPaused: false,
  	modalPause: document.getElementById('modalPause'),
  	modalReset: document.getElementById('modalReset'),
  	resetButton: document.getElementById('softkey-right'),

  	numberBeepValue:document.getElementById('numberBeepValue'),
  	numberBeep: 0,
	
	timeReset: 3000,
	initTime: -3,

  	footerSet: document.getElementById('footerSet'),
  	footerChrono: document.getElementById('footerChrono'),

  	intervalShow: null,
	gongDiv: document.getElementById('gong'),
	randomSound1Div: document.getElementById('randomSound1'),
	randomSound2Div: document.getElementById('randomSound2'),
	randomSound3Div: document.getElementById('randomSound3'),

  	positionNavigator: 0,
  	valueDivSelect: 0,

  	playSound: function(srcSound){
		app.audio.src = "./sound/" + srcSound + ".ogg";
		app.audio.play();
  	},
  
  	init: function() {
    	document.addEventListener('keypress', (e) => {
			app.manejarTeclado(e);
		});
  	},

	initChrono: function() {

		app.positionNavigator = 1;

		setDiv.classList.add("hide");
		chronoDiv.classList.remove("hide");
		app.footerSet.classList.add("hide");
		app.footerChrono.classList.remove("hide");

		//init clock

		/* TODO refactorizar */
		document.getElementsByTagName("html")[0].classList.add('work');
		document.getElementsByClassName("jumbotron")[0].classList.add('work');
		/* TODO refactorizar */

		let restTime = app.workValueInt;
		let initTime = app.initTime;

		app.timeRestInt = restTime;
		app.timeRest.innerText = app.formatTime(app.initTime);

		if (app.numberBeep != 0) {
			app.timeBeep = Math.floor(Math.random() * (app.maxValueInt - app.minValueInt + 1)) + app.minValueInt;
		}
		
		app.interval = setInterval(function() {
			if (initTime < 0) {
				initTime +=1;
				app.timeRestInt = initTime;
			}
			app.timeRest.innerText = app.formatTime(app.timeRestInt);

			if (initTime >= 0 && !app.isPaused) {
				if (restTime == app.workValueInt) {
					app.timeRestInt = restTime;
					app.timeRest.innerText = app.formatTime(app.timeRestInt);				
					app.playSound('gong');
				}
				if (app.numberBeep != 0 && app.timeBeep == 0) {
	  				if (app.numberBeep == 1) {
						app.playSound('alert');
						app.gongDiv.classList.remove('hide');
						clearInterval(app.intervalShow);
						app.intervalShow = setInterval( function() {
							app.gongDiv.classList.add('hide');
						}, 1000);
					} else {
	  					let randomSound = Math.floor(Math.random() * app.numberBeep) + 1;
	  					app.audio.src = "./sound/" + randomSound + ".mp3";
	  					switch (randomSound) {
							case 1:
								app.randomSound1Div.classList.remove('hide');
  								break;
							case 2:								
								app.randomSound2Div.classList.remove('hide');
  								break;
							case 3:
								app.randomSound3Div.classList.remove('hide');
  								break;
							}
						clearInterval(app.intervalShow);
						app.intervalShow = setInterval( function() {
							app.randomSound1Div.classList.add('hide');
							app.randomSound2Div.classList.add('hide');
							app.randomSound3Div.classList.add('hide');
						}, 1000);
						app.audio.play();
	  				}
					app.timeBeep = Math.floor(Math.random() * (app.maxValueInt - app.minValueInt + 1)) + app.minValueInt;
				} else if (app.numberBeep != 0) {
					app.timeBeep -= 1;
				}
	  			if (restTime === 0) {
					//Sumar el tiempo total trabajado
					app.workTime += app.workValueInt;
	  				clearInterval(app.interval);
					app.playSound('gong');
					clearInterval(app.interval);
					restTime = 0;	
					setTimeout(function () {
						app.showConfig();
					}, app.timeReset);									
	  			} else {
					restTime -=1;
				}
				app.timeRestInt = restTime;
  			}
		}, 1000);
	},

	resetChrono: function() {
		app.isPaused = true;

		app.footerSet.classList.add("hide");
		app.footerChrono.classList.add("hide");

		app.modalReset.classList.remove('hide');
		
		if (confirm("Reset. Are sure?") == true) {
			app.isPaused = false;
			app.modalReset.classList.add('hide');

			clearInterval(app.interval);
			app.showConfig();
		}
		else {
			app.isPaused = false;
			app.modalReset.classList.add('hide');
			app.footerChrono.classList.remove("hide");
		}
	},

	resetWorkTime: function() {
		app.modalReset.classList.remove('hide');
		
		if (confirm("Reset. Are sure?") == true) {
			app.modalReset.classList.add('hide');
			app.workTime = 0;
			app.workTimeElement.innerText = app.formatTime(app.workTime);
		}
		else {
			app.modalReset.classList.add('hide');
		}
	},

	showConfig: function() {

		 /* TODO refactorizar */
		document.getElementsByTagName("html")[0].classList.remove('work');
		document.getElementsByClassName("jumbotron")[0].classList.remove('work');
		document.getElementsByTagName("html")[0].classList.remove('rest');
		document.getElementsByClassName("jumbotron")[0].classList.remove('rest');
		 /* TODO refactorizar */
		app.setDiv.classList.remove("hide");
		app.chronoDiv.classList.add("hide");
		app.footerSet.classList.remove("hide");
		app.footerChrono.classList.add("hide");

		app.positionNavigator = 0;

		app.workTimeElement.innerText = app.formatTime(app.workTime);
	},

	pauseChrono: function() {
		app.isPaused = true;
		app.modalPause.classList.remove('hide');
		app.footerSet.classList.remove("hide");
		app.footerChrono.classList.add("hide");
		app.resetButton.classList.add("hidden");
	},

	lessValue: function(type) {
		if (typeof type === "object"){
			type = type.target.getAttribute('data-type');
		}
		switch(type) {
    		case 'sets':
    			app.setsValue.innerText = parseInt(app.setsValue.innerText) - 1;
				if (parseInt(app.setsValue.innerText) < 1) {
					app.setsValue.innerText = 1;
				}
  				break;
  			case 'work':
				if (parseInt(app.workValueInt) < 60) {
					app.workValueInt = parseInt(app.workValueInt) - 5;
				} else if (parseInt(app.workValueInt) < 120) {
					app.workValueInt = parseInt(app.workValueInt) - 10;
				} else if (parseInt(app.workValueInt) < 180) {
					app.workValueInt = parseInt(app.workValueInt) - 15;
				} else if (parseInt(app.workValueInt) <= 300) {
					app.workValueInt = parseInt(app.workValueInt) - 30;
				}
				if (parseInt(app.workValueInt) < 30) {
					app.workValueInt = 30;
				}
				app.workValue.innerText = app.formatTime(app.workValueInt);
  				break;
			case 'min':
				app.minValueInt = parseInt(app.minValueInt) - 1;
				if (parseInt(app.minValueInt) < 2) {
					app.minValueInt = 2;
				}
				app.minValue.innerText = app.formatTime(app.minValueInt);
  				break;
			case 'max':
				app.maxValueInt = parseInt(app.maxValueInt) - 1;
				if (parseInt(app.maxValueInt) < 4) {
					app.maxValueInt = 4;
				}
				app.maxValue.innerText = app.formatTime(app.maxValueInt);
  				break;
		}
	},

	contEnd: function() {
		for (var i = 0; i <= app.intervalButton; i ++) {
			clearInterval(i);
		}
	},

	moreValue: function(type) {
		if (typeof type === "object"){
			type = type.target.getAttribute('data-type');
		}
		switch(type) {
    		case 'sets':
				if (parseInt(app.setsValue.innerText) < 30) {
					app.setsValue.innerText = parseInt(app.setsValue.innerText) + 1;
					if (parseInt(app.setsValue.innerText) < 1) {
						app.setsValue.innerText = 1;
					}
				}
  				break;
  			case 'work':
				if (parseInt(app.workValueInt) < 300) {
					if (parseInt(app.workValueInt) < 60) {
						app.workValueInt = parseInt(app.workValueInt) + 5;
					} else if (parseInt(app.workValueInt) < 120) {
						app.workValueInt = parseInt(app.workValueInt) + 10;
					} else if (parseInt(app.workValueInt) < 180) {
						app.workValueInt = parseInt(app.workValueInt) + 15;
					} else if (parseInt(app.workValueInt) < 300) {
						app.workValueInt = parseInt(app.workValueInt) + 30;
					}
					if (parseInt(app.workValueInt) < 5) {
						app.workValueInt = 5;
					}
					app.workValue.innerText = app.formatTime(app.workValueInt);
				}
				break;
			case 'min':
				if (parseInt(app.minValueInt) < 9) {
					app.minValueInt = parseInt(app.minValueInt) + 1;
					if (parseInt(app.minValueInt) < 2) {
						app.minValueInt = 2;
					}
				}
				app.minValue.innerText = app.formatTime(app.minValueInt);
				break;
			case 'max':
				if (parseInt(app.maxValueInt) < 20) {
					app.maxValueInt = parseInt(app.maxValueInt) + 1;
					if (parseInt(app.maxValueInt) < 4) {
						app.maxValueInt = 4;
					}
				}
				app.maxValue.innerText = app.formatTime(app.maxValueInt);
				break;
		}
	},

  	formatTime: function(time) {
  		if (time < 0) {
  			return "-0:0" + Math.abs(time);
  		} else {
		  	let seg = time%60;
			if (seg < 10) {
				seg = "0" + seg;
			}
			let min = parseInt(time/60);
		  	return min+":"+seg;
	  	}
  	},

	manejarTeclado: function(e) {
		if (e.key == "ArrowUp") { 
			app.valueDivSelect--;
			if (app.valueDivSelect < 0) {
				app.valueDivSelect = 3;
			}

    		for (var i = 0; i < app.valueDivs.length; i++) {
				document.getElementById(app.valueDivs[i]).classList.remove('active');
			}

			document.getElementById(app.valueDivs[app.valueDivSelect]).classList.add('active');
		}
		else if (e.key == "ArrowDown") { 
			app.valueDivSelect++;
			if (app.valueDivSelect > app.valueDivs.length - 1) {
				app.valueDivSelect = 0;
			}			

    		for (var i = 0; i < app.valueDivs.length; i++) {
				document.getElementById(app.valueDivs[i]).classList.remove('active');
			}

			document.getElementById(app.valueDivs[app.valueDivSelect]).classList.add('active');
		}
		else if (e.key == "ArrowLeft") { 

			switch(app.valueDivSelect) {
	    		case 0: 
	    			app.lessValue('work');
	  				break;
	  			case 1:
	    			app.lessValue('min');
	  				break;
				case 2:
	    			app.lessValue('max');
	  				break;
	  			case 3:
	  				app.numberBeep--;
					if (app.numberBeep < 0) {
						app.numberBeep = 3;
					}
					app.numberBeepValue.innerText = app.numberBeep;
	  				break;
  			}
		}
		else if (e.key == "ArrowRight") { 
			switch(app.valueDivSelect) {
	    		case 0: 
	    			app.moreValue('work');
	  				break;
	  			case 1:
	    			app.moreValue('min');
	  				break;
				case 2:
	    			app.moreValue('max');
	  				break;
	  			case 3:
	  				app.numberBeep++;
					if (app.numberBeep > 3) {
						app.numberBeep = 0;
					}
					app.numberBeepValue.innerText = app.numberBeep;
	  				break;
  			}
		}
		else if (e.key == "SoftRight") {
			// Reset
	    	if (app.positionNavigator == 1 && !app.isPaused) {
	    		app.resetChrono();
	    	}	    	
	    	else if (app.positionNavigator == 0) {
	    		app.resetWorkTime();
	    	}	    	
		}	    
		else if (e.key == "SoftLeft") { 
			// Pause
	    	if (app.positionNavigator == 1 && !app.isPaused) {
	    		app.pauseChrono();
	    	}
		}	
    	else if (e.key == "MicrophoneToggle" || e.key == "Enter") {
	    	// Play
	    	if (app.positionNavigator == 0) {
	    		app.initChrono();
	    	}
    		else if (app.isPaused) {	
				app.isPaused = false;
				app.footerSet.classList.add("hide");
				app.footerChrono.classList.remove("hide");
				app.modalPause.classList.add('hide');
				app.resetButton.classList.remove("hidden");
    		}
	    }
	    else if (e.key == "1") {
	    	navigator.volumeManager.requestDown();
	    	navigator.volumeManager.requestShow();
	    }
	    else if (e.key == "3") {
	    	navigator.volumeManager.requestUp();
	    	navigator.volumeManager.requestShow();
	    }
	},
};

document.addEventListener('DOMContentLoaded', function () {
  	app.init();
});

var app = {
  	
  	valueDivs : ["setsValueDiv", "workValueDiv", "restValueDiv"],
  	play: document.getElementById('play'),
  	pause: document.getElementById('pause'),
  	reset: document.getElementById('reset'),
  	setDiv: document.getElementById('setDiv'),
  	chronoDiv: document.getElementById('chronoDiv'),
  	setsValue: document.getElementById('setsValue'),
  	workValue: document.getElementById('workValue'),
  	workValueInt: 30,
  	workFinish: 0,
  	restValue: document.getElementById('restValue'),
  	restValueInt: 15,
  	timeReset: 3000,
  	positionNavigator: 0,
  	state: 1, // 1 -> config, 2 -> chrono
  	stateChrono: 1,  // 1 -> rest, 2 -> work
  	cycle: document.getElementById('cycle'),
  	timeRest: document.getElementById('timeRest'),
  	timeRestInt: 0,
  	textChrono: document.getElementById('textChrono'),
  	interval: null,
  	intervalButton: null,
  	audio: new Audio(),
  	isPaused: false,
  	modalPause: document.getElementById('modalPause'),
  	modalReset: document.getElementById('modalReset'),
  	footerSet: document.getElementById('footerSet'),
  	footerChrono: document.getElementById('footerChrono'),

  	workTimeElement: document.getElementById('workTime'),
  	workTime: 0,

  	valueDivSelect: 0,

  	playSound: function(srcSound){
		app.audio.src = "./sound/" + srcSound + ".ogg";
		app.audio.play();
  	},
  
  	init: function() {
    	document.addEventListener('keypress', (e) => {
				app.manejarTeclado(e);
			});

			app.updateWorkTime();
  	},

	initChrono: function() {
		app.positionNavigator = 1;
		setDiv.classList.add("hide");
		chronoDiv.classList.remove("hide");
		app.footerSet.classList.add("hide");
		app.footerChrono.classList.remove("hide");
		app.state = 2;
		cycle.innerText = (app.workFinish + 1) + "/" + app.setsValue.innerText;
		//app.timeRest.innerText =  10; // app.restValue.innerText;
		document.getElementById('principalDiv').classList.add('rest');

		//init clock		
		app.textChrono.innerText = "Rest"; /* TODO refactorizar */
		/* TODO refactorizar */
		document.getElementsByTagName("html")[0].classList.add('rest');
		document.getElementsByClassName("jumbotron")[0].classList.add('rest');
		/* TODO refactorizar */
		let restTime = 5; //app.restValue.innerText;
		app.timeRestInt = restTime;
		app.timeRest.innerText = app.formatTime(app.timeRestInt);
		app.interval = setInterval(function() {
			if (!app.isPaused) {
	  			if (restTime === 0) {
	  				if (app.stateChrono === 1) {  			
						restTime = app.workValueInt;
						app.stateChrono = 2;
						app.textChrono.innerText = "Work"; /* TODO refactorizar */
						/* TODO refactorizar */
						document.getElementsByTagName("html")[0].classList.remove('rest');
						document.getElementsByClassName("jumbotron")[0].classList.remove('rest');
						document.getElementsByTagName("html")[0].classList.add('work');
						document.getElementsByClassName("jumbotron")[0].classList.add('work');
						/* TODO refactorizar */
	  					app.playSound('alert');
	  				} else {
	  					restTime = app.restValueInt;
	  					app.stateChrono = 1;
	  					app.textChrono.innerText = "Rest"; /* TODO refactorizar */
	  					app.workFinish += 1;  							
	  					if (app.workFinish >= app.setsValue.innerText){
	  						clearInterval(app.interval);
	  						app.playSound('gong');
							app.textChrono.innerText = "Finish !!!"; /* TODO refactorizar */
							clearInterval(app.interval);
							restTime = 0;
	    					setTimeout(function () {
								app.showConfig();
	    					}, app.timeReset);
	  					} else {
		  					cycle.innerText = (app.workFinish + 1) + "/" + app.setsValue.innerText;
		  					/* TODO refactorizar */
		  					document.getElementsByTagName("html")[0].classList.remove('work');
		  					document.getElementsByClassName("jumbotron")[0].classList.remove('work');
		  					document.getElementsByTagName("html")[0].classList.add('rest');
		  					document.getElementsByClassName("jumbotron")[0].classList.add('rest');
		  					/* TODO refactorizar */
		  					app.playSound('gong2');
						}
	  				}
	  			} else {
					restTime -=1;
				}
				app.timeRestInt = restTime;
				app.timeRest.innerText = app.formatTime(app.timeRestInt);
	  			if (restTime === 3) { 
	  				app.playSound('end');
	  			}
  			}
		}, 1000);
	},

	resetChrono: function() {
			app.isPaused = true;

			app.footerSet.classList.add("hide");
			app.footerChrono.classList.add("hide");

			app.modalReset.classList.remove('hide');
				
			if (confirm("Reset. Are sure?") == true) {
				app.isPaused = false;
				app.modalReset.classList.add('hide');

				clearInterval(app.interval);
				app.stateChrono = 1;
				app.workFinish = 0;
				app.showConfig();
			}
			else {
				app.isPaused = false;
				app.modalReset.classList.add('hide');
				app.footerChrono.classList.remove("hide");
			}
		
	},

	updateWorkTime: function() {
  		app.workTime = (app.workValueInt * parseInt(app.setsValue.innerText) ) + (app.restValueInt * (parseInt(app.setsValue.innerText) - 1) );
		app.workTimeElement.innerText = app.formatTime(app.workTime);
	},

	showConfig: function() {
		 /* TODO refactorizar */
		document.getElementsByTagName("html")[0].classList.remove('work');
		document.getElementsByClassName("jumbotron")[0].classList.remove('work');
		document.getElementsByTagName("html")[0].classList.remove('rest');
		document.getElementsByClassName("jumbotron")[0].classList.remove('rest');
		 /* TODO refactorizar */
		app.setDiv.classList.remove("hide");
		app.chronoDiv.classList.add("hide");
		app.footerSet.classList.remove("hide");
		app.footerChrono.classList.add("hide");
		app.state = 1;
		app.positionNavigator = 0;
		app.workFinish = 0;
	},

	pauseChrono: function() {
			app.isPaused = true;
			app.modalPause.classList.remove('hide');
			app.footerSet.classList.remove("hide");
			app.footerChrono.classList.add("hide");		
	},

	lessValue: function(type) {
		if (typeof type === "object"){
			type = type.target.getAttribute('data-type');
		}
		switch(type) {
    		case 'sets':
    			app.setsValue.innerText = parseInt(app.setsValue.innerText) - 1;
				if (parseInt(app.setsValue.innerText) < 1) {
					app.setsValue.innerText = 1;
				}
  				break;
  			case 'work':
				if (parseInt(app.workValueInt) < 60) {
					app.workValueInt = parseInt(app.workValueInt) - 5;
				} else if (parseInt(app.workValueInt) < 120) {
					app.workValueInt = parseInt(app.workValueInt) - 10;
				} else if (parseInt(app.workValueInt) < 180) {
					app.workValueInt = parseInt(app.workValueInt) - 15;
				} else if (parseInt(app.workValueInt) <= 300) {
					app.workValueInt = parseInt(app.workValueInt) - 30;
				}
				if (parseInt(app.workValueInt) < 5) {
					app.workValueInt = 5;
				}
				app.workValue.innerText = app.formatTime(app.workValueInt);
  				break;
			case 'rest':
				if (parseInt(app.restValueInt) < 60) {
					app.restValueInt = parseInt(app.restValueInt) - 5;					
				} else if (parseInt(app.restValueInt) <= 120) {
					app.restValueInt = parseInt(app.restValueInt) - 10;
				}
				if (parseInt(app.restValueInt) < 5) {
					app.restValueInt = 3;
				}
				app.restValue.innerText = app.formatTime(app.restValueInt);
  				break;
		}
		app.updateWorkTime();
	},

	moreValue: function(type) {
		if (typeof type === "object"){
			type = type.target.getAttribute('data-type');
		}
		switch(type) {
    		case 'sets':
				if (parseInt(app.setsValue.innerText) < 30) {
					app.setsValue.innerText = parseInt(app.setsValue.innerText) + 1;
					if (parseInt(app.setsValue.innerText) < 1) {
						app.setsValue.innerText = 1;
					}
				}
  				break;
  			case 'work':
				if (parseInt(app.workValueInt) < 300) {
					if (parseInt(app.workValueInt) < 60) {
						app.workValueInt = parseInt(app.workValueInt) + 5;
					} else if (parseInt(app.workValueInt) < 120) {
						app.workValueInt = parseInt(app.workValueInt) + 10;
					} else if (parseInt(app.workValueInt) < 180) {
						app.workValueInt = parseInt(app.workValueInt) + 15;
					} else if (parseInt(app.workValueInt) < 300) {
						app.workValueInt = parseInt(app.workValueInt) + 30;
					}
					if (parseInt(app.workValueInt) < 5) {
						app.workValueInt = 5;
					}
					app.workValue.innerText = app.formatTime(app.workValueInt);
				}
				break;
			case 'rest':
				if (parseInt(app.restValueInt) < 120) {
					if (parseInt(app.restValueInt) < 60) {
						if (parseInt(app.restValueInt) < 5) {
							app.restValueInt = 5;
						} else {
							app.restValueInt = parseInt(app.restValueInt) + 5;
						}
					} else if (parseInt(app.restValueInt) < 120) {
						app.restValueInt = parseInt(app.restValueInt) + 10;
					}
					if (parseInt(app.restValueInt) < 5) {
						app.restValueInt = 3;
					}
				}
				app.restValue.innerText = app.formatTime(app.restValueInt);
				break;
		}		
		app.updateWorkTime();
	},

	manejarTeclado: function(e) {
		if (e.key == "ArrowUp") { 
			app.valueDivSelect--;
			if (app.valueDivSelect < 0) {
				app.valueDivSelect = 2;
			}

    	for (var i = 0; i < app.valueDivs.length; i++) {
				document.getElementById(app.valueDivs[i]).classList.remove('active');
			}

			document.getElementById(app.valueDivs[app.valueDivSelect]).classList.add('active');
		}
		else if (e.key == "ArrowDown") { 
			app.valueDivSelect++;
			if (app.valueDivSelect > app.valueDivs.length - 1) {
				app.valueDivSelect = 0;
			}			

    	for (var i = 0; i < app.valueDivs.length; i++) {
				document.getElementById(app.valueDivs[i]).classList.remove('active');
			}

			document.getElementById(app.valueDivs[app.valueDivSelect]).classList.add('active');
		}
		else if (e.key == "ArrowLeft") { 

			switch(app.valueDivSelect) {
    		case 0: 
    			app.lessValue('sets');
  				break;
  			case 1:
    			app.lessValue('work');
  				break;
				case 2:
    			app.lessValue('rest');
  				break;
  		}
		}
		else if (e.key == "ArrowRight") { 
			switch(app.valueDivSelect) {
    		case 0: 
    			app.moreValue('sets');
  				break;
  			case 1:
    			app.moreValue('work');
  				break;
				case 2:
    			app.moreValue('rest');
  				break;
  		}
		}
		else if (e.key == "SoftRight") {
			// Reset
    	if (app.positionNavigator == 1 && !app.isPaused) {
    		app.resetChrono();
    	}
		}	    
		else if (e.key == "SoftLeft") { 
			// Pause
    	if (app.positionNavigator == 1 && !app.isPaused) {
    		app.pauseChrono();
    	}
		}	
    else if (e.key == "MicrophoneToggle") {
    	// Play
    	if (app.positionNavigator == 0) {
    		app.initChrono();
    	}
    	else if (app.isPaused) {	
				app.isPaused = false;
				app.footerSet.classList.add("hide");
				app.footerChrono.classList.remove("hide");
				app.modalPause.classList.add('hide');
    	}
    }
    else if (e.key == "1") {
    	navigator.volumeManager.requestDown();
    	navigator.volumeManager.requestShow();
    }
    else if (e.key == "3") {
    	navigator.volumeManager.requestUp();
    	navigator.volumeManager.requestShow();
    }
	},

  formatTime: function(time) {
  	let seg = time%60;
	if (seg < 10) {
		seg = "0" + seg;
	}
	let min = parseInt(time/60);
  	return min+":"+seg;
  }
};

document.addEventListener('DOMContentLoaded', function () {
  	app.init();
});

var app = {
  	nameLocalStorage: '_randomExercise',
	modalReset: document.getElementById('modalReset'),
	modalPause: document.getElementById('modalPause'),
	textExercise: document.getElementById('textExercise'),
	footer: document.getElementById('footer'),

  	exercises: [
  		"FLEXIONES",
  		"ABDOMINALES",
  		"SENTADILLAS",
  		"TRIPCES",
  		"ZANCADAS",
  		"BURPEES",
  		"PESO MUERTO",
  		"BICEPS",
  		"GOMA",
  		"SERIE PUÑOS",
  		"RUEDA"
  	],
  
  	init: function() {
    	document.addEventListener('keypress', (e) => {
			app.manejarTeclado(e);
		});
  	},

	resetExercises: function() {
			app.modalReset.classList.remove('hide');
			app.footer.classList.add("hide");

			if (confirm("¿Estás seguro?") == true) {
				app.modalReset.classList.add('hide');
				app.footer.classList.remove("hide");
			    localStorage.removeItem(app.nameLocalStorage);
			}
			else {
				app.modalPause.classList.add('hide');
				app.footer.classList.remove("hide");
			}
	},

	calculateDoExercise: function() {

		let saveExercice = localStorage.getItem(app.nameLocalStorage);		
		let saveExerciceParseToday = null;
		let yourDate = new Date();

		if(saveExercice !== undefined && saveExercice !== null) {		
			let saveExerciceParse = JSON.parse(saveExercice);
			saveExerciceParseToday = saveExerciceParse.find(x => x.date === yourDate.toISOString().split('T')[0])
			if(saveExerciceParseToday !== undefined && saveExerciceParseToday !== null) {
				if (saveExerciceParseToday.exercises.length >= app.exercises.length) {					
					app.modalPause.classList.remove('hide');
					app.footer.classList.add("hide");

					alert("Has hecho todos hoy,\n pulsa reset si quieres hacer más");
					app.modalPause.classList.add('hide');
					app.footer.classList.remove("hide");

					return;
				}
			}
		}		

		let exerciseIndex = Math.floor(Math.random() * app.exercises.length);

		if(saveExercice === undefined || saveExercice === null) {			
			localStorage.setItem(app.nameLocalStorage, JSON.stringify([{'date': yourDate.toISOString().split('T')[0], 'exercises': [exerciseIndex] }]));
		} else {
			if(saveExerciceParseToday !== undefined && saveExerciceParseToday !== null) {
				if (saveExerciceParseToday.exercises.find(x => x === exerciseIndex))
				{					
					exerciseIndex = app.checkExercise(app.exercises.length, saveExerciceParseToday.exercises);
					saveExerciceParseToday.exercises.push(exerciseIndex);
					localStorage.setItem(app.nameLocalStorage, JSON.stringify([{'date': yourDate.toISOString().split('T')[0], 'exercises': saveExerciceParseToday.exercises }]));
				} else {
					saveExerciceParseToday.exercises.push(exerciseIndex);
					localStorage.setItem(app.nameLocalStorage, JSON.stringify([{'date': yourDate.toISOString().split('T')[0], 'exercises': saveExerciceParseToday.exercises }]));
				}
			}
		}

		app.textExercise.innerText = app.exercises[exerciseIndex];
	},

	checkExercise: function(lengthExercise, exercisesToday) {
		let exerciseIndex = Math.floor(Math.random() * lengthExercise);
		if (exercisesToday.find(x => x === exerciseIndex)) {
			exerciseIndex = app.checkExercise(lengthExercise, exercisesToday)
		}
		return exerciseIndex;
	},

	manejarTeclado: function(e) {
		if (e.key == "SoftRight") {
			app.calculateDoExercise();
		} else if (e.key == "SoftLeft") {
			app.resetExercises();
		}
	},
};
